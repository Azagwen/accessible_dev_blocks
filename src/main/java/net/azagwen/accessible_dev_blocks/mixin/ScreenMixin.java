package net.azagwen.accessible_dev_blocks.mixin;

import net.azagwen.accessible_dev_blocks.AdbMain;
import net.azagwen.accessible_dev_blocks.screen.AdbOptionColorScreen;
import net.azagwen.accessible_dev_blocks.screen.AdbOptionScreen;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArgs;
import org.spongepowered.asm.mixin.injection.invoke.arg.Args;

@Mixin(Screen.class)
public class ScreenMixin {

    @ModifyArgs(
            method = "renderBackgroundTexture(I)V",
            at = @At(value = "INVOKE", target = "Lcom/mojang/blaze3d/systems/RenderSystem;setShaderTexture(ILnet/minecraft/util/Identifier;)V")
    )
    public void redirectBindTexture(Args args) {
        if (MinecraftClient.getInstance().currentScreen instanceof AdbOptionScreen || MinecraftClient.getInstance().currentScreen instanceof AdbOptionColorScreen) {
            args.set(1, AdbMain.id("textures/gui/config_background.png"));
        }
    }
}
